package back02productosmongodb.controlador;

import back02productosmongodb.modelo.ProductoModel;
import back02productosmongodb.servicio.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController @RequestMapping("/apiprodleo/v2")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/producto")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @GetMapping("/producto/{id}" )
    public Optional<ProductoModel> getProductoId(@PathVariable String id){
        return productoService.findById(id);
    }

    @PostMapping("/producto")
    public ProductoModel postProductos(@RequestBody ProductoModel newProducto){
        productoService.save(newProducto);
        return newProducto;
    }

    @PutMapping("/producto")
    public void putProductos(@RequestBody ProductoModel productoToUpdate){
        productoService.save(productoToUpdate);
    }

    @DeleteMapping("/producto")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete){
        return productoService.delete(productoToDelete);
    }
}