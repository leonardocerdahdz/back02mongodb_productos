package back02productosmongodb.servicio;
import back02productosmongodb.modelo.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ProductoRepository extends MongoRepository<ProductoModel, String> { }



